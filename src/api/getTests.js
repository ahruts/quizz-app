const testsApi = () => fetch('/tests.json').then(response => response.json())

export default testsApi;