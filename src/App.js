import "./App.css";
import Card from "./components/card";
import { useEffect, useState } from "react";
import getTests from "./api/getTests";

function App() {
  const [tests, setTests] = useState([]);
  const [activeStep, setActiveStep] = useState(0);
  const [correctAnswers, setCorrectAnswers] = useState(0);
  const [correctAnswersText, setCorrectAnswersText] = useState([]);

  function handleClick(correctAnswer, answer) {
    if (correctAnswer) {
      setCorrectAnswers((prev) => prev + 1);
      const answerCurrent = `<div style="color:green">${answer}</div>`;
      setCorrectAnswersText((prev) => [...prev, answerCurrent]);
    } else {
      const answerCurrent = `<div style="color:red">${answer}</div>`;
      setCorrectAnswersText((prev) => [...prev, answerCurrent]);
    }

    setTimeout(() => {
      setActiveStep(activeStep + 1);
    }, 1000);
  }

  useEffect(() => {
    getTests().then((response) => {
      setTests(response.test1);
      console.log(response.test1[activeStep]);
    });
  }, []);

  return activeStep < tests.length ? (
    <>
      {tests ? (
        <Card onClick={handleClick} data={tests[activeStep]}></Card>
      ) : (
        <div>Loading...</div>
      )}
      <footer>
        {activeStep + 1}/{tests && tests.length}
      </footer>
    </>
  ) : (
    <div>
      <p>
        Your score is {correctAnswers}/ {tests.length}
      </p>
      {correctAnswersText.map((item) => `${item}`)}
      <button onClick={() => setActiveStep(0)}>Retry</button>
    </div>
  );
}
export default App;
