import { useEffect, useState } from "react";
const Card = ({ data: { question, answers }, onClick }) => {
  const [isSubmited, setIsSubmited] = useState(false);
  const [isSelected, setIsSelected] = useState(null);

  useEffect(() => {
    setIsSubmited(false);
  }, [answers]);

  const formHandler = (e) => {
    e.preventDefault();
    setIsSubmited(true);
    answers.forEach((item) => {
      if (item.answer === isSelected && item.correct) {
        onClick(true, isSelected);
      } else {
        onClick(false, isSelected);
      }
    });
  };

  const radioHandler = (event) => {
    setIsSelected(event.target.value);
    console.log(event.target);
  };

  const changeColor = (item) => {
    if (isSubmited && isSelected === item.answer && item.correct) {
      console.log(true);
    }
    if (isSubmited && isSelected === item.answer && !item.correct) {
      return {
        color: "red",
      };
    }
    if (isSubmited && item.correct) {
      return {
        color: "green",
      };
    }
  };

  return (
    <>
      <h2>{question}</h2>
      <form onSubmit={formHandler}>
        <div onChange={radioHandler}>
          {answers.map((item, index) => {
            return (
              <div key={item.answer}>
                <input
                  disabled={isSubmited}
                  type="radio"
                  id={index}
                  name="answers"
                  value={item.answer}
                />
                <label htmlFor={index} style={changeColor(item)}>
                  {item.answer}
                </label>
              </div>
            );
          })}
        </div>
        <button disabled={isSubmited} type="submit">
          Submit
        </button>
      </form>
    </>
  );
};

export default Card;
